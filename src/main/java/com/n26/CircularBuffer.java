package com.n26;

import com.n26.entities.Metrics;
import com.n26.entities.Transaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.ZonedDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * CircularBuffer is an infinite type of array, after reaching the last position it goes back to the first one.
 * https://en.wikipedia.org/wiki/Circular_buffer
 *
 * This implementation has 60 positions, each position represents one second.
 * And for each second we have the corresponding transactions. Every second we move the cursor forward.
 * To retrieve data for the last 60s all that is required is to read all the information in the buffer.
 */
public class CircularBuffer {
    private AtomicInteger currentIndex = new AtomicInteger(0);
    private AtomicReferenceArray<List<Transaction>> buffer;
    private final int bufferSize;

    public CircularBuffer() {
        this.bufferSize = 60;
        buffer = new AtomicReferenceArray<>(this.bufferSize);
    }

    /**
     * Add a transaction to the buffer.
     * To find the right position the index is relative to the seconds of the given transaction.
     *
     * @param transaction
     */
    public void addTransaction(final Transaction transaction) {
        int seconds = transaction.getTimestampAsDateTime().getSecond();
        int index = bufferSize - ZonedDateTime.now().minusSeconds(seconds).getSecond() + currentIndex.get();
        if (index >= bufferSize) {
            index = index - bufferSize;
        }
        List<Transaction> transactions = buffer.get(index);
        if (transactions == null) {
            transactions = new LinkedList<>();
        }
        transactions.add(transaction);
        buffer.set(index, transactions);
    }

    /**
     * Calculate metrics for the given transactions
     * @see Metrics
     * @return metrics of all the transactions stored.
     */
    public Metrics getMetrics() {
        Metrics metrics = new Metrics();
        long count = 0;
        BigDecimal sum = BigDecimal.ZERO.setScale(2, RoundingMode.HALF_UP);
        BigDecimal max = null;
        BigDecimal min = null;

        for (int i = 0; i < bufferSize; i++) {
            List<Transaction> transactions = buffer.get(i);
            if (transactions == null) continue;

            for (Transaction transaction : transactions) {
                BigDecimal amount = transaction.getAmountAsBigDecimal().setScale(2, RoundingMode.HALF_UP);

                count++;
                sum = sum.add(amount);
                if (max == null || max.compareTo(amount) == -1) {
                    max = amount;
                }

                if (min == null || min.compareTo(amount) == 1) {
                    min = amount;
                }
            }
        }

        if (count > 0 && sum.compareTo(BigDecimal.ZERO) == 1) {
            metrics.setAvg(sum.divide(new BigDecimal(count), RoundingMode.HALF_UP));
        } else {
            metrics.setAvg(BigDecimal.ZERO);
        }
        metrics.setCount(count);
        metrics.setSum(sum);
        metrics.setMax(max == null ? BigDecimal.ZERO : max);
        metrics.setMin(min == null ? BigDecimal.ZERO : min);
        return metrics;
    }

    /**
     * Move the buffer to the next cursor
     * This will destroy the new index data.
     */
    public int nextIndex() {
        int newIndex = currentIndex.incrementAndGet();
        if (newIndex >= bufferSize) {
            newIndex = 0;
            currentIndex.set(newIndex);
        }
        buffer.set(newIndex, null);
        return newIndex;
    }

    /**
     * Clear all the data in the buffer
     */
    public void reset() {
        buffer = new AtomicReferenceArray<>(bufferSize);
        currentIndex.set(0);
    }
}
