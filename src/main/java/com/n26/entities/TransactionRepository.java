package com.n26.entities;

import com.n26.CircularBuffer;

/**
 * Keeps an in memory repository for all transactions
 */
public class TransactionRepository {
    private static TransactionRepository instance = null;
    private CircularBuffer buffer;
    private Metrics metrics;

    private TransactionRepository() {
        buffer = new CircularBuffer();
        metrics = new Metrics();
    }

    public synchronized void addTransaction(Transaction transaction) {
        buffer.addTransaction(transaction);
        metrics = buffer.getMetrics();
    }

    public synchronized int nextIndex() {
        int newIndex = buffer.nextIndex();
        metrics = buffer.getMetrics();

        return newIndex;
    }

    public synchronized void reset() {
        buffer.reset();
        metrics = new Metrics();
    }

    public synchronized Metrics getMetrics() {
        return metrics;
    }

    public static synchronized TransactionRepository getInstance() {
        if (instance == null) {
            instance = new TransactionRepository();
        }

        return instance;
    }
}
