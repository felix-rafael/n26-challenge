package com.n26.entities;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class Transaction {
    private String amount;
    private BigDecimal amountAsBigDecimal = null;
    private String timestamp;

    public Transaction() {
    }

    public Transaction(String amount, String timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public Transaction(String amount, ZonedDateTime timestamp) {
        this.amount = amount;
        this.timestamp = timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
    }

    public String getAmount() {
        return amount;
    }

    /**
     * Convert the input value into BigDecimal
     * @return the big decimal reference, null if missing or invalid.
     */
    public BigDecimal getAmountAsBigDecimal() {
        if (amount == null) return null;

        if (amountAsBigDecimal == null ) {
            try {
                amountAsBigDecimal = new BigDecimal(amount);
            } catch (NumberFormatException ex) {
                return null;
            }
        }

        return amountAsBigDecimal;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public ZonedDateTime getTimestampAsDateTime() {
        if (timestamp == null) return null;

        try {
            return ZonedDateTime.parse(timestamp);
        } catch (DateTimeParseException ex) {
            return null;
        }
    }
}
