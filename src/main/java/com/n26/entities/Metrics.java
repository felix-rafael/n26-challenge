package com.n26.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.n26.util.MetricValueSerializer;

import java.math.BigDecimal;

public class Metrics {
    @JsonSerialize(using = MetricValueSerializer.class)
    private BigDecimal sum = BigDecimal.ZERO;
    @JsonSerialize(using = MetricValueSerializer.class)
    private BigDecimal avg = BigDecimal.ZERO;
    @JsonSerialize(using = MetricValueSerializer.class)
    private BigDecimal max = BigDecimal.ZERO;
    @JsonSerialize(using = MetricValueSerializer.class)
    private BigDecimal min = BigDecimal.ZERO;
    private long count = 0;

    public Metrics() {
    }

    public BigDecimal getSum() {
        return sum;
    }

    public void setSum(BigDecimal sum) {
        this.sum = sum;
    }

    public BigDecimal getAvg() {
        return avg;
    }

    public void setAvg(BigDecimal avg) {
        this.avg = avg;
    }

    public BigDecimal getMax() {
        return max;
    }

    public void setMax(BigDecimal max) {
        this.max = max;
    }

    public BigDecimal getMin() {
        return min;
    }

    public void setMin(BigDecimal min) {
        this.min = min;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
