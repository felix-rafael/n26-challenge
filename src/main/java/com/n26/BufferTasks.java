package com.n26;

import com.n26.entities.TransactionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
public class BufferTasks {
    private static final Logger log = LoggerFactory.getLogger(BufferTasks.class);

    @Scheduled(fixedRate = 1000)
    public void moveBufferCursor() {
        int index = TransactionRepository.getInstance().nextIndex();
        log.info("Updating buffer index to " + index);
    }
}
