package com.n26.controllers;

import com.n26.entities.TransactionRepository;
import com.n26.entities.Metrics;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MetricsController {
    @GetMapping(value = "/statistics", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Metrics getMetrics() {
        return TransactionRepository.getInstance().getMetrics();
    }
}
