package com.n26.controllers;

import com.n26.entities.TransactionRepository;
import com.n26.entities.Transaction;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.time.ZonedDateTime;

@Controller
public class TransactionsController {
    @PostMapping(value = "/transactions", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createTransaction(@RequestBody Transaction transaction) {
        if (transaction.getTimestampAsDateTime() == null || transaction.getAmountAsBigDecimal() == null) {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if(transaction.getTimestampAsDateTime().isAfter(ZonedDateTime.now())) {
            return new ResponseEntity(HttpStatus.UNPROCESSABLE_ENTITY);
        }
        if (transaction.getTimestampAsDateTime().isBefore(ZonedDateTime.now().minusSeconds(60))) {
            return new ResponseEntity(HttpStatus.NO_CONTENT);
        }

        TransactionRepository.getInstance().addTransaction(transaction);
        return new ResponseEntity(HttpStatus.CREATED);
    }

    @DeleteMapping("/transactions")
    public ResponseEntity clearTransactions() {
        TransactionRepository.getInstance().reset();
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
