package com.n26;

import com.n26.entities.Metrics;
import com.n26.entities.Transaction;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;

public class CircularBufferTest {
    @Test
    public void addTransactionsAndReturnMetrics() {
        CircularBuffer cb = new CircularBuffer();
        cb.addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(2)));
        cb.addTransaction(new Transaction("150.00", ZonedDateTime.now().minusSeconds(1)));
        Metrics metrics = cb.getMetrics();
        assertEquals(2, metrics.getCount());
        assertEquals(new BigDecimal("250.00"), metrics.getSum());
        assertEquals(new BigDecimal("150.00"), metrics.getMax());
        assertEquals(new BigDecimal("100.00"), metrics.getMin());
        assertEquals(new BigDecimal("125.00"), metrics.getAvg());
    }

    @Test
    public void roundTheMetricsCorrectly() {
        CircularBuffer cb = new CircularBuffer();
        cb.addTransaction(new Transaction("5", ZonedDateTime.now().minusSeconds(2)));
        cb.addTransaction(new Transaction("3", ZonedDateTime.now().minusSeconds(1)));
        cb.addTransaction(new Transaction("3", ZonedDateTime.now().minusSeconds(3)));

        Metrics metrics = cb.getMetrics();
        assertEquals(3, metrics.getCount());
        assertEquals(new BigDecimal("11.00"), metrics.getSum());
        assertEquals(new BigDecimal("5.00"), metrics.getMax());
        assertEquals(new BigDecimal("3.00"), metrics.getMin());
        assertEquals(new BigDecimal("3.67"), metrics.getAvg());
    }

    @Test
    public void circlesTheTransactionsResetingTheIndexAccordingly() {
        CircularBuffer cb = new CircularBuffer();
        for(int i = 0; i < 50; i++) {
            cb.nextIndex();
        }

        cb.addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(40)));
        Metrics metrics = cb.getMetrics();
        assertEquals(1, metrics.getCount());
    }

    @Test
    public void cleanTheNextPosition() {
        CircularBuffer cb = new CircularBuffer();
        for(int i = 0; i < 60; i++) {
            cb.addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(i)));
        }
        cb.nextIndex();
        Metrics metrics = cb.getMetrics();
        assertEquals(59, metrics.getCount());
        assertEquals(new BigDecimal("5900.00"), metrics.getSum());
        assertEquals(new BigDecimal("100.00"), metrics.getMax());
        assertEquals(new BigDecimal("100.00"), metrics.getMin());
        assertEquals(new BigDecimal("100.00"), metrics.getAvg());
    }

    @Test
    public void resetsTheIndexToZero() {
        CircularBuffer cb = new CircularBuffer();
        for(int i = 0; i < 60; i++) {
            cb.nextIndex();
        }
        cb.nextIndex();
        cb.addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(1)));
        Metrics metrics = cb.getMetrics();
        assertEquals(1, metrics.getCount());
    }

    @Test
    public void clearAllTheTransactions() {
        CircularBuffer cb = new CircularBuffer();
        for(int i = 0; i < 60; i++) {
            cb.addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(i)));
        }
        Metrics metrics = cb.getMetrics();
        assertEquals(60, metrics.getCount());
        cb.reset();
        metrics = cb.getMetrics();
        assertEquals(0, metrics.getCount());
    }
}
