package com.n26.entities;

import org.junit.Test;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class TransactionTest {
    @Test
    public void returnsNullAmountAsBigDecimalWhenAmountIsNull() {
        Transaction t = new Transaction(null, "");
        assertNull(t.getAmountAsBigDecimal());

    }
    @Test
    public void returnsNullAmoutAsBigDecimalWhenAmountIsInvalidNumber() {
        Transaction t = new Transaction("abc", "");
        assertNull(t.getAmountAsBigDecimal());
    }

    @Test
    public void returnsAmountAsBigDecimalWhenAmoutIsValid() {
        Transaction t = new Transaction("123.12", "");
        assertEquals(new BigDecimal("123.12"), t.getAmountAsBigDecimal());
    }

    @Test
    public void returnsNullTimestampAsDateTimeWhenTimestampIsNull() {
        Transaction t = new Transaction(null, "");
        assertNull(t.getTimestampAsDateTime());
    }

    @Test
    public void returnsNullTimestampAsDateTimeWhenTimestampIsInvalidFormat() {
        Transaction t = new Transaction(null, "4/23/2013 04:53");
        assertNull(t.getTimestampAsDateTime());
    }

    @Test
    public void returnsTimestampAsDateTimeWhenTimestampIsValid() {
        ZonedDateTime dateTime = ZonedDateTime.now();
        Transaction t = new Transaction(null, dateTime.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME));
        assertEquals(dateTime.toLocalDateTime(), t.getTimestampAsDateTime().toLocalDateTime());
    }
}
