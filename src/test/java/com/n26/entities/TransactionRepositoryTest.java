package com.n26.entities;

import org.junit.Before;
import org.junit.Test;

import java.time.ZonedDateTime;

import static org.junit.Assert.*;

public class TransactionRepositoryTest {
    private TransactionRepository repository = TransactionRepository.getInstance();
    @Before
    public void setup() {
        repository.reset();
    }

    @Test
    public void updateMetricsWhenATransactionIsAdded() {
        assertEquals(0, repository.getMetrics().getCount());
        repository.addTransaction(new Transaction("123.12", ZonedDateTime.now()));
        assertEquals(1, repository.getMetrics().getCount());
    }

    @Test
    public void updateMetricsWhenIndexChange() {
        assertEquals(0, repository.getMetrics().getCount());
        for(int i = 0; i < 60; i++) {
            repository.addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(i)));
        }
        assertEquals(60, repository.getMetrics().getCount());
        repository.nextIndex();
        assertEquals(59, repository.getMetrics().getCount());
    }

    @Test
    public void resetMetricsWhenResetIsCalled() {
        assertEquals(0, repository.getMetrics().getCount());
        for(int i = 0; i < 60; i++) {
            repository.addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(i)));
        }
        assertEquals(60, repository.getMetrics().getCount());
        repository.reset();
        assertEquals(0, repository.getMetrics().getCount());
    }
}
