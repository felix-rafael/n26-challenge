package com.n26.controllers;

import com.n26.entities.TransactionRepository;
import com.n26.entities.Metrics;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class CreateTransactionsTest {
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = standaloneSetup(new TransactionsController()).build();
        TransactionRepository.getInstance().reset();
    }

    @Test
    public void returnsCreatedWhenTransactionIsValid() throws Exception {
        ZonedDateTime timestamp = ZonedDateTime.now().minusSeconds(5);
        String formattedTimestamp = timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        String json = "{ \"amount\": \"1231.12\", \"timestamp\": \"" + formattedTimestamp + "\" }";
        this.mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is(HttpStatus.CREATED.value()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(1, metrics.getCount());
    }

    @Test
    public void returnsNoContentWhenTransactionIsOlderThan60seconds() throws Exception {
        ZonedDateTime timestamp = ZonedDateTime.now().minusSeconds(61);
        String formattedTimestamp = timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        String json = "{ \"amount\": \"1231.12\", \"timestamp\": \"" + formattedTimestamp + "\" }";
        this.mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is(HttpStatus.NO_CONTENT.value()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(0, metrics.getCount());
    }

    @Test
    public void returnsUnprocessableEntitytWhenTimeStampIsMissing() throws Exception {
        String json = "{ \"amount\": \"1231.12\" }";
        this.mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(0, metrics.getCount());
    }

    @Test
    public void returnsUnprocessableEntityWhenAmountIsMissing() throws Exception {
        ZonedDateTime timestamp = ZonedDateTime.now().minusSeconds(5);
        String formattedTimestamp = timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        String json = "{ \"timestamp\": \"" + formattedTimestamp + "\" }";
        this.mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(0, metrics.getCount());
    }

    @Test
    public void returnsBadRequestWhenJsonIsInvalid() throws Exception {
        String json = "NOT A JSON";
        this.mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(0, metrics.getCount());
    }

    @Test
    public void returnsUnprocessableEntityIftransactionIsInTheFuture() throws Exception {
        ZonedDateTime timestamp = ZonedDateTime.now().plusSeconds(2);
        String formattedTimestamp = timestamp.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME);
        String json = "{ \"amount\": \"1231.12\", \"timestamp\": \"" + formattedTimestamp + "\" }";
        this.mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(0, metrics.getCount());
    }

    @Test
    public void parseDatesInDifferentFormats() throws Exception {
        String json = "{ \"amount\": \"1231.12\", \"timestamp\": \"4/23/2018 11:32 PM\" }";
        this.mockMvc
                .perform(post("/transactions").contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().is(HttpStatus.UNPROCESSABLE_ENTITY.value()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(0, metrics.getCount());
    }
}
