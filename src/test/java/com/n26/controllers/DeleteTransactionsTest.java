package com.n26.controllers;

import com.n26.entities.TransactionRepository;
import com.n26.entities.Metrics;
import com.n26.entities.Transaction;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class DeleteTransactionsTest {
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = standaloneSetup(new TransactionsController()).build();
    }

    @Test
    public void returnsNoContentForAnyRequest() throws Exception {
        this.mockMvc
                .perform(delete("/transactions"))
                .andExpect(status().is(HttpStatus.NO_CONTENT.value()));
    }

    @Test()
    public void clearAllStoredTransactions() throws Exception {
        TransactionRepository.getInstance().addTransaction(new Transaction("123", ZonedDateTime.now()));
        Metrics metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(1, metrics.getCount());
        this.mockMvc
                .perform(delete("/transactions"))
                .andExpect(status().is(HttpStatus.NO_CONTENT.value()));
        metrics = TransactionRepository.getInstance().getMetrics();
        assertEquals(0, metrics.getCount());
    }
}
