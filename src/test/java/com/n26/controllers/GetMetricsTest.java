package com.n26.controllers;

import com.n26.entities.TransactionRepository;
import com.n26.entities.Transaction;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import java.time.ZonedDateTime;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

public class GetMetricsTest {
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = standaloneSetup(new MetricsController()).build();
        TransactionRepository.getInstance().reset();
    }

    @Test
    public void returnsZeroValuesWhenNoTransactionIsAvailable() throws Exception {
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/statistics").accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
        JSONObject object = new JSONObject(response.getContentAsString());
        assertEquals(0, object.get("count"));
        assertEquals("0.00", object.get("sum"));
        assertEquals("0.00", object.get("avg"));
        assertEquals("0.00", object.get("min"));
        assertEquals("0.00", object.get("max"));
    }

    @Test
    public void returnsTheMetricsForTheStoredTransactions() throws Exception {
        TransactionRepository.getInstance().addTransaction(new Transaction("100.00", ZonedDateTime.now().minusSeconds(2)));
        TransactionRepository.getInstance().addTransaction(new Transaction("150.00", ZonedDateTime.now().minusSeconds(1)));
        TransactionRepository.getInstance().addTransaction(new Transaction("50.00", ZonedDateTime.now().minusSeconds(3)));
        MockHttpServletResponse response = this.mockMvc
                .perform(get("/statistics").accept(MediaType.APPLICATION_JSON))
                .andReturn()
                .getResponse();

        assertEquals(response.getStatus(), HttpStatus.OK.value());
        JSONObject object = new JSONObject(response.getContentAsString());
        assertEquals(3, object.get("count"));
        assertEquals("300.00", object.get("sum"));
        assertEquals("100.00", object.get("avg"));
        assertEquals("50.00", object.get("min"));
        assertEquals("150.00", object.get("max"));
    }
}
